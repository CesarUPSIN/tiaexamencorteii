const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

router.get("/paginaEntrada", (req, res)=> {
    const params = {
        txtNumContrato: req.query.txtNumContrato,
        txtNombre: req.query.txtNombre,
        txtDomicilio: req.query.txtDomicilio,
        optNivel: req.query.optNivel,
        txtPagoDiario: req.query.txtPagoDiario,
        txtDiasTrabajados: req.query.txtDiasTrabajados,
    }
    res.render("paginaEntrada.html", params);
});

router.post("/paginaEntrada", (req, res)=> {
    const params = {
        txtNumContrato: req.body.txtNumContrato,
        txtNombre: req.body.txtNombre,
        txtDomicilio: req.body.txtDomicilio,
        optNivel: req.body.optNivel,
        txtPagoDiario: req.body.txtPagoDiario,
        txtDiasTrabajados: req.body.txtDiasTrabajados,
    }
    res.render("paginaEntrada.html", params);
});

router.get("/paginaSalida", (req, res)=> {
    const params = {
        txtNumContrato: req.query.txtNumContrato,
        txtNombre: req.query.txtNombre,
        txtDomicilio: req.query.txtDomicilio,
        optNivel: req.query.optNivel,
        txtPagoDiario: req.query.txtPagoDiario,
        txtDiasTrabajados: req.query.txtDiasTrabajados,
    }
    res.render("paginaSalida.html", params);
});

router.post("/paginaSalida", (req, res)=> {
    const params = {
        txtNumContrato: req.body.txtNumContrato,
        txtNombre: req.body.txtNombre,
        txtDomicilio: req.body.txtDomicilio,
        optNivel: req.body.optNivel,
        txtPagoDiario: req.body.txtPagoDiario,
        txtDiasTrabajados: req.body.txtDiasTrabajados,
    }
    res.render("paginaSalida.html", params);
});

module.exports = router;